package org.uma.pab.factorial;

/**
 * Class to compute the factorial of an integer number
 * @author ajnebro
 */
public class Factorial {

  /**
   * Function to compute the factorial of a number
   * @param value
   * @return
   */
	public int compute(int value) {
		int result ;

		if (value < 0) {
		  throw new FactorialException("Value is negagtive: " + value) ;
		} else if ((value == 0) || (value == 1)) {
			result = 1;
		} else {
			result = value * compute(value - 1) ;
		}

		return result ;
	}

  /**
   * Method returning a string
   * @return
   */
	public String toString() {
	  return "Factorial" ;
  }
}
