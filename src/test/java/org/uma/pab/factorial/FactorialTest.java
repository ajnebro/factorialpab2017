package org.uma.pab.factorial;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FactorialTest {
	private Factorial factorial ;
	
	@Before
	public void start() {
		factorial = new Factorial() ;
	}
	
	@Test
	public void shouldFactorialOfZeroReturnOne() {
		int expectedValue = 1 ;
		int receivedValue = factorial.compute(0) ;
		
		assertEquals(expectedValue, receivedValue) ;
	}
	
	@Test
	public void shouldFactorialOfOneReturnOne() {
		int expectedValue = 1 ;
		int receivedValue = factorial.compute(1) ;
		
		assertEquals(expectedValue, receivedValue) ;
	}
	
	@Test
	public void shouldFactorialOfTwoReturnTwo() {
		int expectedValue = 2 ;
		int receivedValue = factorial.compute(2) ;
		
		assertEquals(expectedValue, receivedValue) ;
	}
	
	@Test
	public void shouldFactorialOfThreeReturnSix() {
		int expectedValue = 6 ;
		int receivedValue = factorial.compute(3) ;
		
		assertEquals(expectedValue, receivedValue) ;
	}
	
	@Test
	public void shouldFactorialOfFiveReturn120() {
		int expectedValue = 120 ;
		int receivedValue = factorial.compute(5) ;
		
		assertEquals(expectedValue, receivedValue) ;
	}

	@Test (expected = FactorialException.class)
	public void shouldFactorialOfANegativeNumberRaiseAnException() {
		factorial.compute(-1) ;
	}
}